package speedhome.interview.boot.service;

import speedhome.interview.boot.dto.BookDto;

public interface BookService {
    BookDto findBookById(Long id);

    void addBook(BookDto dto);

    void removeById(Long id);

    void updateBook(BookDto dto);

    void borrowBook(Long id) throws Exception;

    void returnBook(Long id) throws Exception;
}
