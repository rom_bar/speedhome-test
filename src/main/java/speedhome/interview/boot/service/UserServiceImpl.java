package speedhome.interview.boot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import speedhome.interview.boot.dto.MemberDto;
import speedhome.interview.boot.entity.User;
import speedhome.interview.boot.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserDetailsService, UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found with username: " + username));

        return org.springframework.security.core.userdetails.User
                .withUsername(user.getUsername())
                .password(user.getPassword())
                .roles(user.getRole().name())
                .build();
    }

    @Override
    public void addMember(MemberDto dto) {
        var user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void updateMember(MemberDto dto) {
        userRepository.updateUsernameById(dto.getId(), dto.getUsername());
    }

    @Override
    public MemberDto viewMemberById(Long id) {
        var user = userRepository.findById(id)
                .orElseThrow();
        return MemberDto.builder()
                .id(user.getId())
                .username(user.getUsername())
                .build();
    }

    @Override
    public void removeMemberById(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeMemberByUsername(String username) {
        userRepository.deleteByUsername(username);
    }
}
