package speedhome.interview.boot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import speedhome.interview.boot.constant.BookStatus;
import speedhome.interview.boot.dto.BookDto;
import speedhome.interview.boot.entity.Book;
import speedhome.interview.boot.exception.BadRequestException;
import speedhome.interview.boot.repository.BookRepository;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository repository;

    @Override
    public BookDto findBookById(Long id) {
        var book = repository.getById(id);
        return BookDto.builder()
                .id(book.getId())
                .author(book.getAuthor())
                .title(book.getTitle())
                .status(book.getStatus())
                .build();
    }

    @Override
    public void addBook(BookDto dto) {
        var book = new Book();
        book.setAuthor(dto.getAuthor());
        book.setTitle(dto.getTitle());
        book.setStatus(BookStatus.AVAILABLE);
        repository.save(book);
    }

    @Override
    public void removeById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public void updateBook(BookDto dto) {
        var book = repository.findById(dto.getId())
                .orElseThrow();
        book.setAuthor(dto.getAuthor());
        book.setStatus(dto.getStatus());
        book.setTitle(dto.getTitle());
        repository.save(book);
    }

    @Override
    public void borrowBook(Long id) throws Exception {
        var book = repository.findById(id)
                .orElseThrow();
        if (book.getStatus() != BookStatus.AVAILABLE)
            throw new BadRequestException("Book not available");
        book.setStatus(BookStatus.BORROWED);
        repository.save(book);
    }

    @Override
    public void returnBook(Long id) throws Exception {
        var book = repository.findById(id)
                .orElseThrow();
        if (book.getStatus() != BookStatus.BORROWED)
            throw new BadRequestException("Book is not borrowed");
        book.setStatus(BookStatus.AVAILABLE);
        repository.save(book);
    }
}
