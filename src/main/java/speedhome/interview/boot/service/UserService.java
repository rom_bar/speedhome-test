package speedhome.interview.boot.service;

import speedhome.interview.boot.dto.MemberDto;

public interface UserService {
    void addMember(MemberDto dto);

    void updateMember(MemberDto dto);

    MemberDto viewMemberById(Long id);

    void removeMemberById(Long id);

    void removeMemberByUsername(String username);
}
