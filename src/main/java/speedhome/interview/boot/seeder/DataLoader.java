package speedhome.interview.boot.seeder;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import speedhome.interview.boot.constant.UserRole;
import speedhome.interview.boot.entity.User;
import speedhome.interview.boot.repository.UserRepository;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) {
        User user = new User();
        user.setUsername("user");
        user.setPassword(passwordEncoder.encode("password"));
        user.setRole(UserRole.LIBRARIAN);
        userRepository.save(user);
        User asd = new User();
        asd.setUsername("member");
        asd.setPassword(passwordEncoder.encode("password"));
        asd.setRole(UserRole.MEMBER);
        userRepository.save(asd);
    }
}

