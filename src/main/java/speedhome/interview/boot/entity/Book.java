package speedhome.interview.boot.entity;

import lombok.Getter;
import lombok.Setter;
import speedhome.interview.boot.constant.BookStatus;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String author;
    @Enumerated(EnumType.STRING)
    private BookStatus status;
}
