package speedhome.interview.boot.entity;

import lombok.Getter;
import lombok.Setter;
import speedhome.interview.boot.constant.UserRole;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    @Enumerated(EnumType.STRING)
    private UserRole role;
}

