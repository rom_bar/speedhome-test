package speedhome.interview.boot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import speedhome.interview.boot.service.UserService;

@RestController
@RequiredArgsConstructor
public class MemberController {
    private final UserService service;

    @DeleteMapping("/member/account")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeOwnAccount(@AuthenticationPrincipal UserDetails userDetails) {
        service.removeMemberByUsername(userDetails.getUsername());
    }
}
