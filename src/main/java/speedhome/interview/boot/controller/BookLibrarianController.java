package speedhome.interview.boot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import speedhome.interview.boot.dto.BookDto;
import speedhome.interview.boot.service.BookService;

@RestController
@RequestMapping("/librarian/books")
@RequiredArgsConstructor
public class BookLibrarianController {
    private final BookService bookService;

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addBook(@RequestBody BookDto book) {
        bookService.addBook(book);
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void updateBook(@PathVariable Long id, @RequestBody BookDto book) {
        book.setId(id);
        bookService.updateBook(book);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeBook(@PathVariable Long id) {
        bookService.removeById(id);
    }
}


