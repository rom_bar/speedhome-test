package speedhome.interview.boot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import speedhome.interview.boot.dto.BookDto;
import speedhome.interview.boot.dto.CommonResponseDto;
import speedhome.interview.boot.service.BookService;

@RestController
@RequestMapping("/member/books")
@RequiredArgsConstructor
public class BookMemberController {
    private final BookService service;

    @GetMapping("/{id}")
    public ResponseEntity<CommonResponseDto<BookDto>> viewBook(@PathVariable Long id) {
        var book = service.findBookById(id);
        var result = new CommonResponseDto<BookDto>();
        result.setMessage("Success");
        result.setData(book);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/borrow/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void borrowBook(@PathVariable Long id) throws Exception {
        service.borrowBook(id);
    }

    @PostMapping("/return/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void returnBook(@PathVariable Long id) throws Exception {
        service.returnBook(id);
    }
}