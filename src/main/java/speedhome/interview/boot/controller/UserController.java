package speedhome.interview.boot.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import speedhome.interview.boot.dto.CommonResponseDto;
import speedhome.interview.boot.dto.MemberDto;
import speedhome.interview.boot.service.UserService;

@RestController
@RequestMapping("/librarian/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService service;

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public void addMember(@RequestBody MemberDto dto) {
        service.addMember(dto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommonResponseDto<MemberDto>> viewMemberById(@PathVariable Long id) {
        var member = service.viewMemberById(id);
        var result = new CommonResponseDto<MemberDto>();
        result.setMessage("Success");
        result.setData(member);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeMemberById(@PathVariable Long id) {
        service.removeMemberById(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void updateMemberById(@PathVariable Long id, MemberDto memberDto) {
        memberDto.setId(id);
        service.updateMember(memberDto);
    }
}
