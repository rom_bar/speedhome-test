package speedhome.interview.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import speedhome.interview.boot.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

    Optional<User> findById(Long id);

    @Modifying
    @Query("update User u set u.username = :username where u.id = :id")
    void updateUsernameById(Long id, String username);

    void deleteByUsername(String username);
}