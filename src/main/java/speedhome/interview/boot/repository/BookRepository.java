package speedhome.interview.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import speedhome.interview.boot.entity.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
}
