package speedhome.interview.boot.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import speedhome.interview.boot.constant.BookStatus;

@Setter
@Getter
@Builder
public class BookDto {
    private Long id;
    private String title;
    private String author;
    private BookStatus status;
}
