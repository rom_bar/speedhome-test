package speedhome.interview.boot.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommonResponseDto<T> {
    private String message;
    private String errorMessage;
    private T data;
}
