package speedhome.interview.boot.constant;

public enum UserRole {
    LIBRARIAN,
    MEMBER
}
