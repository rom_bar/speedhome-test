package speedhome.interview.boot.constant;

public enum BookStatus {
    AVAILABLE,
    BORROWED
}
