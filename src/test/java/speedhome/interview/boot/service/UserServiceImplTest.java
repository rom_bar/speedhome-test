package speedhome.interview.boot.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import speedhome.interview.boot.dto.MemberDto;
import speedhome.interview.boot.entity.User;
import speedhome.interview.boot.repository.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {
    @Mock
    private UserRepository repository;
    @Mock
    private PasswordEncoder passwordEncoder;
    private UserService userService;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(repository, passwordEncoder);
    }

    @Test
    void addMember() {
        //Given
        var dto = MemberDto.builder()
                .username("cukup")
                .password("cukup123")
                .build();
        //When
        when(repository.save(any()))
                .thenReturn(new User());
        //Assert
        assertDoesNotThrow(() -> userService.addMember(dto));
    }

    @Test
    void updateMember() {
        //Given
        var dto = MemberDto.builder()
                .id(1L)
                .username("bisa")
                .password("bisa123")
                .build();
        //When
        doNothing().when(repository).updateUsernameById(anyLong(), anyString());
        //Assert
        assertDoesNotThrow(() -> userService.updateMember(dto));
    }

    @Test
    void viewMemberById() {
        //Given
        var user = new User();
        user.setUsername("Hiccup");
        user.setId(1L);
        //When
        when(repository.findById(anyLong()))
                .thenReturn(Optional.of(user));
        //Assert
        var result = userService.viewMemberById(1L);
        assertEquals("Hiccup", result.getUsername());
        assertEquals(1L, result.getId());
    }

    @Test
    void removeMemberById() {
        //Given
        var id = 1L;
        //When
        doNothing().when(repository).deleteById(anyLong());
        //Assert
        assertDoesNotThrow(() -> userService.removeMemberById(id));
    }

    @Test
    void removeMemberByUsername() {
        //Given
        var username = "aku";
        //When
        doNothing().when(repository).deleteByUsername(anyString());
        //Assert
        assertDoesNotThrow(() -> userService.removeMemberByUsername(username));

    }
}