package speedhome.interview.boot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import speedhome.interview.boot.dto.BookDto;
import speedhome.interview.boot.service.BookService;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class BookLibrarianControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BookService bookService;

    @BeforeEach
    void setUp() {
        bookService.addBook(
                BookDto.builder()
                        .title("Super")
                        .author("Prost")
                        .build());
    }

    @Test
    void removeBook() throws Exception {
        var headers = getHttpHeaders();

        mockMvc.perform(MockMvcRequestBuilders.delete("/librarian/books/1")
                        .headers(headers))
                .andExpect(status().isNoContent());
    }

    @Test
    void updateBook() throws Exception {
        var book = BookDto.builder()
                .title("Mechanism")
                .author("Me")
                .build();
        var headers = getHttpHeaders();

        mockMvc.perform(MockMvcRequestBuilders.put("/librarian/books/1")
                        .headers(headers)
                        .content(new ObjectMapper().writeValueAsString(book)))
                .andExpect(status().isNoContent());
    }

    @Test
    void addBook() throws Exception {
        var book = BookDto.builder()
                .title("Cool")
                .author("Me")
                .build();

        var headers = getHttpHeaders();

        mockMvc.perform(MockMvcRequestBuilders.post("/librarian/books")
                        .headers(headers)
                        .content(new ObjectMapper().writeValueAsString(book)))
                .andExpect(status().isCreated());
    }

    private HttpHeaders getHttpHeaders() {
        var headers = new HttpHeaders();
        headers.setBasicAuth("user", "password");
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}
