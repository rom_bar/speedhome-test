package speedhome.interview.boot.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import speedhome.interview.boot.dto.MemberDto;
import speedhome.interview.boot.service.UserServiceImpl;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class UserControllerTest {

    @Autowired
    private UserServiceImpl service;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void addMember() throws Exception {
        var member = MemberDto.builder()
                .username("satu")
                .password("satu123")
                .build();

        var headers = getLibrarianHeader();

        mockMvc.perform(MockMvcRequestBuilders.post("/librarian/users")
                        .headers(headers)
                        .content(new ObjectMapper().writeValueAsString(member)))
                .andExpect(status().isCreated());
    }

    @Test
    void getMember() throws Exception {
        var member = MemberDto.builder()
                .username("aku")
                .password("aku123")
                .build();
        service.addMember(member);

        var headers = getLibrarianHeader();

        mockMvc.perform(MockMvcRequestBuilders.get("/librarian/users/3")
                        .headers(headers))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.message", Matchers.is("Success")))
                .andExpect(jsonPath("$.data.id", Matchers.is(3)))
                .andExpect(jsonPath("$.data.username", Matchers.is("aku")));
    }

    @Test
    void updateMember() throws Exception {
        var member = MemberDto.builder()
                .username("dua")
                .password("dua123")
                .build();

        var headers = getLibrarianHeader();

        mockMvc.perform(MockMvcRequestBuilders.put("/librarian/users/2")
                        .headers(headers)
                        .content(new ObjectMapper().writeValueAsString(member)))
                .andExpect(status().isNoContent());
    }

    @Test
    void deleteMember() throws Exception {
        var headers = getLibrarianHeader();

        mockMvc.perform(MockMvcRequestBuilders.delete("/librarian/users/2")
                        .headers(headers))
                .andExpect(status().isNoContent());
    }

    private HttpHeaders getLibrarianHeader() {
        var headers = new HttpHeaders();
        headers.setBasicAuth("user", "password");
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}