package speedhome.interview.boot.controller;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.transaction.annotation.Transactional;
import speedhome.interview.boot.dto.BookDto;
import speedhome.interview.boot.exception.BadRequestException;
import speedhome.interview.boot.service.BookService;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class BookMemberControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private BookService bookService;

    @BeforeEach
    void setUp() {
        bookService.addBook(
                BookDto.builder()
                        .title("MemberOnly")
                        .author("Prost")
                        .build());
    }

    @Test
    @Transactional
    void viewBook() throws Exception {
        var headers = getHttpHeaders();

        mockMvc.perform(MockMvcRequestBuilders.get("/member/books/1")
                        .headers(headers))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json"))
                .andExpect(jsonPath("$.message", Matchers.is("Success")))
                .andExpect(jsonPath("$.data.title", Matchers.is("MemberOnly")))
                .andExpect(jsonPath("$.data.author", Matchers.is("Prost")));

    }

    @Test
    void borrowBook() throws Exception {
        var headers = getHttpHeaders();

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/member/books/borrow/1")
                        .headers(headers))
                .andExpect(status().isNoContent());
    }

    @Test
    void returnBook() throws Exception {
        var headers = getHttpHeaders();
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/member/books/return/1")
                        .headers(headers))
                .andExpect(status().isBadRequest())
                .andExpect(mvcResult -> assertTrue(mvcResult.getResolvedException() instanceof BadRequestException))
                .andExpect(mvcResult -> assertEquals("Book is not borrowed", Objects.requireNonNull(mvcResult.getResolvedException()).getMessage()));


        mockMvc.perform(MockMvcRequestBuilders
                        .post("/member/books/borrow/1")
                        .headers(headers))
                .andExpect(status().isNoContent());

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/member/books/return/1")
                        .headers(headers))
                .andExpect(status().isNoContent());

    }

    private HttpHeaders getHttpHeaders() {
        var headers = new HttpHeaders();
        headers.setBasicAuth("member", "password");
        headers.setContentType(MediaType.APPLICATION_JSON);
        return headers;
    }
}